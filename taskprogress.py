
from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication

class TaskProgress:
    def __init__(self):
        self.mainui = loadUi('taskprogress.ui')
        self.mainui.show()
        self.mainui.barMeasure.setValue(0)
        self.mainui.chkLearning.stateChanged.connect(self.progress)
        self.mainui.chkCleaning.stateChanged.connect(self.progress)
        self.mainui.chkPlaying.stateChanged.connect(self.progress)
        
    def progress(self):
        completed_task = sum([self.mainui.chkLearning.isChecked(), \
                              self.mainui.chkCleaning.isChecked(), \
                              self.mainui.chkPlaying.isChecked()])
        tasknum = 3
        progress_status = (completed_task / tasknum) * 100
        self.mainui.barMeasure.setValue(int(progress_status))
        
        if int(progress_status) == 100:
            self.mainui.lblMsg.setText("The Progress Bar is Fully 100% now")
        else:
            self.mainui.lblMsg.setText("Msg")

if __name__ == '__main__':
    app = QApplication([])
    main = TaskProgress()
    app.exec()